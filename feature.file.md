## Scenario 1
### TC1: Helyes összeadás
    given: Elindult az alkalmazás

    when: Beírom az összeadni kívánt tört számokat megfelelő alakban
    then: Eredményként a helyes összeget kapom

### TC2: Helytelen összeadás
    given: Elindult az alkalmazás

    when: Beírom az összeadni kívánt tört számokat helytelenül
    then: Eredményként hiba üzenetet kapok: "Invalid value"

## Scenario 2
### TC1: Helyes kivonás
    given: Elindult az alkalmazás

    when: Beírom a kivonandó tört számokat megfelelő alakban
    then: Eredményként a helyes értéket kapom

### TC2: Helytelen kivonás
    given: Elindult az alkalmazás

    when: Beírom a kivonandó tört számokat helytelen alakban
    then: Eredményként hiba üzenetet kapok: "Invalid value"

## Scenario 3
### TC1: Helyes szorzás
    given: Elindult az alkalmazás

    when: Beírom a szorozni kívánt tört számokat megfelelő alakban
    then: Eredményként a helyes értéket kapom

### TC2: Helytelen szorzás
    given: Elindult az alkalmazás

    when: Beírom a szorozni kívánt tört számokat helytelenül
    then: Eredményként hiba üzenetet kapok: "Invalid value"

## Scenario 4
### TC1: Helyes osztás
    given: Elindult az alkalmazás

    when: Beírom az osztandó és osztó tört számokat megfelelő alakban
    then: Eredményként a helyes értéket kapom

### TC2: Helytelen osztás
    given: Elindult az alkalmazás

    when: Beírom az osztandó és osztó tört számokat helytelenül
    then: Eredményként hiba üzenetet kapok: "Invalid value"

## Scenario 5
### TC1: Helyes hatványozás
    given: Elindult az alkalmazás

    when: Beírom a hatványozni kívánt tört számot és a kitevőt megfelelő alakban
    then: Eredményként a helyes értéket kapom

### TC2: Helytelen hatványozás
    given: Elindult az alkalmazás

    when: Beírom a hatványozni kívánt tört számot helyes alakban és a kitevőt helytelen alakban
    then: Eredményként hiba üzenetet kapok: "Invalid value"

### TC3: Helytelen hatványozás
    given: Elindult az alkalmazás

    when: Beírom a hatványozni kívánt tört számot helytelen alakban és a kitevőt helyes alakban
    then: Eredményként hiba üzenetet kapok: "Invalid value"
