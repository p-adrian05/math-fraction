package math;

/**
 * Class for representing fractions that are not reduced automatically.
 */
public class Fraction {

    /**
     * The numerator of this fraction.
     */
    protected final int numerator;

    /**
     * The denominator of this fraction that is always positive.
     */
    protected final int denominator;

    /**
     * The {@code Fraction} object that represents the number zero.
     */
    public static final Fraction ZERO = new Fraction(0);

    /**
     * The {@code Fraction} object that represents the number one.
     */
    public static final Fraction ONE = new Fraction(1);

    /**
     * Constructs a {@code Fraction} object.
     *
     * @param numerator the numerator of the fraction
     * @param denominator the denominator of the fraction
     * @throws ArithmeticException if the denominator is zero
     */
    public Fraction(int numerator, int denominator) throws ArithmeticException {
        if (denominator == 0) {
            throw new ArithmeticException("Division by zero");
        }
        if (denominator > 0) {
            this.numerator = numerator;
            this.denominator = denominator;
        } else {
            this.numerator = -numerator;
            this.denominator = -denominator;
        }
    }

    /**
     * Constructs a {@code Fraction} object that represents the {@code int}
     * specified.
     *
     * @param n the value to be represented
     */
    public Fraction(int n) {
        this(n, 1);
    }

    /**
     * Returns the numerator of this fraction.
     *
     * @return the numerator of this fraction
     */
    public int getNumerator() {
        return numerator;
    }

    /**
     * Returns the denominator of this fraction.
     *
     * @return the denominator of this fraction
     */
    public int getDenominator() {
        return denominator;
    }

    /**
     * Raises this fraction to the power specified.
     *
     * @param n the exponent
     * @return a fraction that represents the result
     */
    public Fraction pow(int n) {
        if(n == 0)
            return new Fraction(1,1);

        boolean isNegativeExpo = n < 0;
        n = n < 0 ? -n : n;

        int numerator = this.numerator;
        int denominator = this.denominator;

        while (n != 1){
            numerator *= this.numerator;
            denominator *= this.denominator;
            n--;
        }

        if(isNegativeExpo)
            return new Fraction(denominator, numerator);
        return new Fraction(numerator, denominator);
    }

    /**
     * Adds a fraction to this fraction.
     *
     * @param fraction the fraction to add
     * @return a fraction that represents the result
     */
    public Fraction add(Fraction fraction) {
        if(denominator==fraction.denominator)
            return new Fraction(numerator-fraction.numerator ,denominator);
        return new Fraction(numerator*fraction.denominator+fraction.numerator*denominator, denominator*fraction.denominator);
    }

    /**
     * Subtracts a fraction from this fraction.
     *
     * @param fraction the fraction to subtract
     * @return a fraction that represents the result
     */
    public Fraction subtract(Fraction fraction) {
        if(denominator==fraction.denominator)
            return new Fraction(numerator-fraction.numerator ,denominator);
        return new Fraction(numerator*fraction.denominator-fraction.numerator*denominator, denominator*fraction.denominator);
    }

    /**
     * Divides this fraction by another.
     *
     * @param fraction the fraction to divide by
     * @return a fraction that represents the result
     * @throws ArithmeticException if the parameter {@code fraction} is zero
     */
    public Fraction divide(Fraction fraction) throws ArithmeticException {
        if(fraction.equals(Fraction.ZERO)){
            throw new ArithmeticException("Division by zero");
        }
        return new Fraction(numerator*fraction.denominator,denominator*fraction.numerator);

    }
    /**
     * Divides this fraction by an {@code int}.
     *
     * @param n the {@code int} to divide by
     * @return a fraction that represents the result
     * @throws ArithmeticException if the parameter {@code n} is zero
     */
    public Fraction divide(int n) throws ArithmeticException {
        if(n ==0){
            throw new ArithmeticException("Parameter is zero");
        }
        else{
            return new Fraction(numerator,denominator*n);
        }
    }

    /**
     * Multiplies this fraction by another.
     *
     * @param fraction the fraction to multiply by
     * @return a fraction that represents the result
     */
    public Fraction multiply(Fraction fraction) {
        if(this.denominator == 0 || fraction == Fraction.ZERO){
            return Fraction.ZERO;
        }
        return new Fraction(numerator*fraction.numerator,denominator*fraction.denominator);
    }

}