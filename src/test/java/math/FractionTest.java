package math;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class FractionTest {

    private void assertFraction(int expectedNumerator, int expectedDenominator, Fraction actual) {
        assertAll("fraction",
                () -> assertEquals(expectedNumerator, actual.getNumerator(), "Invalid numerator"),
                () -> assertEquals(expectedDenominator, actual.getDenominator(), "Invalid denominator")
        );
    }

    @Test
    @DisplayName("Simple power operation should work")
    public void testPow() {
        Fraction f = new Fraction(2, 5);
        assertFraction(1, 1, f.pow(0));
        assertFraction(4, 25, f.pow(2));
        assertFraction(25, 4, f.pow(-2));
    }

    @Test
    @DisplayName("Simple add operation should work")
    public void testAddFraction() {
        Fraction f = new Fraction(12, 17);
        assertFraction(13, 17, f.add(new Fraction(1, 17)));
        assertFraction(41, 34, f.add(new Fraction(1, 2)));
    }

    @Test
    @DisplayName("Simple subtract operation should work")
    public void testSubtractFraction() {
        Fraction f = new Fraction(12, 17);
        assertFraction(11, 17, f.subtract(new Fraction(1, 17)));
        assertFraction(7, 34, f.subtract(new Fraction(1, 2)));
    }

    @Test
    @DisplayName("Simple divide operation should work")
    public void testDivideFraction() {
        Fraction f = new Fraction(-21, 7);
        assertFraction(147, 147, f.divide(f));

    }
    @Test
    @DisplayName("Simple multiplication operation should work")
    public void testMultiplyFraction() {
        Fraction f = new Fraction(11, 13);
        assertSame(Fraction.ZERO, f.multiply(Fraction.ZERO));
        assertFraction(121, 169, f.multiply(f));
        assertFraction(143, 143, f.multiply(new Fraction(13, 11)));
    }

    @Test
    @DisplayName("Simple divide operation with integer should work")
    public void testDivideInt() {
        Fraction f = new Fraction(8, 3);
        assertFraction(8, 12, f.divide(4));
        assertFraction(-8, 33, f.divide(-11));
        assertThrows(ArithmeticException.class, () -> f.divide(0));
    }

    @Test
    @DisplayName("Ensure correct handling of zero")
    public void testDivideWithZero() {
        Fraction f = new Fraction(8, 3);
        assertThrows(ArithmeticException.class, () -> f.divide(Fraction.ZERO));
    }
}
